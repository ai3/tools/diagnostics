import contextlib
import mechanicalsoup
import requests
from ai_diagnostics.adapters import ForcedIPHTTPSAdapter, LoggingHTTPAdapter


@contextlib.contextmanager
def make_session(ctx):
    session = requests.Session()

    # Install DNS overrides, or just a simple logging adapter.
    dns_map = ctx.get('dns', {})
    if dns_map:
        for name, ip in dns_map.items():
            adapter = ForcedIPHTTPSAdapter(ctx=ctx, dest_ip=ip)
            session.mount('https://' + name, adapter)
    else:
        session.mount('https://', LoggingHTTPAdapter(ctx=ctx))

    try:
        yield session
    finally:
        session.close()


@contextlib.contextmanager
def make_browser(ctx):
    with make_session(ctx) as s:

        browser = mechanicalsoup.StatefulBrowser(
            soup_config={'features': 'lxml'},
            raise_on_404=True,
            session=s,
        )
        browser.set_verbose(2)

        # Print a dump of browser state on errors.
        try:
            yield browser
        except Exception as e:
            ctx.log('ERROR: %s', e)
            ctx.log('Browser URL: %s\n\nPage contents:\n\n%s',
                    browser.get_url(),
                    browser.get_current_page())
            raise
        finally:
            browser.close()


class LoginError(Exception):
    pass


def _login_error(page):
    errp = page.find('p', class_='error')
    if not errp:
        return None
    return errp.string.strip()


def _is_login_page(browser):
    url = browser.get_url().split('?')[0]
    return url.endswith('/sso/login')


def _is_2fa_form(page):
    return (page.find('input', id='u2fResponseField') is not None)


def open_url(ctx, browser, url):
    """Open a URL, transparently handling authentication."""
    resp = browser.open(url, timeout=60)
    ctx.log('requesting %s', browser.get_url())
    if not _is_login_page(browser):
        return resp

    if 'credentials' not in ctx:
        raise LoginError('Authentication required but no credentials provided')

    browser.select_form()
    browser['username'] = ctx['credentials']['username']
    browser['password'] = ctx['credentials']['password']
    ctx.log('login page detected, logging in as %s...', ctx['credentials']['username'])
    resp.close()
    resp = browser.submit_selected()

    page = browser.get_current_page()
    err_msg = _login_error(page)
    if err_msg:
        raise LoginError(err_msg)
    elif _is_2fa_form(page):
        raise LoginError('2FA required')
    else:
        ctx.log('login successful')

    return resp
