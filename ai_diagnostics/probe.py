import io
import logging
import time
try:
    from prometheus_client import Gauge
    probe_success = Gauge('probe_success', 'Probe success', ['probe', 'probeset'])
    probe_last_run_ts = Gauge(
        'probe_last_run_seconds', 'Timestamp of last probe run', ['probe', 'probeset'])
    has_prometheus = True
except ImportError:
    has_prometheus = False


# A way to distinguish these prober metrics from all others.
PROBESET = 'service'


class Context(dict):

    def copy(self):
        return Context(self.items())

    def with_logger(self, name, buffer):
        ctx = Context(self.items())
        h = logging.StreamHandler(buffer)
        h.setFormatter(
            logging.Formatter('%(asctime)s - %(message)s'))
        ctx._log = logging.getLogger(name)
        ctx._log.addHandler(h)
        ctx._log.setLevel(logging.DEBUG)
        return ctx
    
    def log(self, fmt, *args):
        self._log.info(fmt, *args)


class Probe(object):

    def __init__(self, fn, name, ctx):
        self.fn = fn
        self.name = name
        self.probeset = PROBESET

        # Create a logger for this probe, and make a copy of the Context.
        if not isinstance(ctx, Context):
            ctx = Context(ctx.items())
        self.ctx = ctx

    def __str__(self):
        return self.name

    def __call__(self, results):
        start_timestamp = time.time()
        log_buffer = io.StringIO()
        ctx = self.ctx.with_logger(self.name, log_buffer)
        try:
            result = {'status': 'ok'}
            probe_data = self.fn(ctx)
            if probe_data:
                result.update(probe_data)
        except BaseException as e:
            err_str = '%s: %s' % (e.__class__.__name__, str(e))
            ctx.log('ERROR: %s', err_str)
            logging.exception('error in probe %s: %s', self.name, err_str)
            result = {'status': 'error',
                      'error': err_str}
        end_timestamp = time.time()
        result['timestamp'] = end_timestamp
        result['elapsed'] = end_timestamp - start_timestamp
        result['log'] = log_buffer.getvalue()
        if has_prometheus:
            probe_success.labels(self.name, self.probeset).set(
                1 if result['status'] == 'ok' else 0)
            probe_last_run_ts.labels(self.name, self.probeset).set(end_timestamp)
        results.add(self.name, int(end_timestamp), result)


def parameterize(probe, attr, values):
    """Return multiple copies of the probe, with 'attr' set to each of 'values'.

    The first argument can be a list of probes, for simple
    cross-composition of nested parameterize() calls.

    """
    out = []
    probes_list = probe
    if not isinstance(probe, list):
        probes_list = [probe]
    for probe in probes_list:
        for v in values:
            name = '%s/%s=%s' % (probe.name, attr, v)
            ctx = probe.ctx.copy()
            ctx[attr] = v
            out.append(Probe(probe.fn, name, ctx))
    return out
