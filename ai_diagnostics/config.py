import glob
import logging
import os

from ai_diagnostics.probe import Probe, parameterize
from ai_diagnostics.probes.pannello import probe_pannello
from ai_diagnostics.probes.webmail import probe_webmail
from ai_diagnostics.probes.imap import probe_imap
from ai_diagnostics.probes.smtp import *
from ai_diagnostics.probes.webdav import probe_webdav
from ai_diagnostics.probes.xmpp import probe_xmpp


class Credentials(dict):

    def __str__(self):
        return self['username']


class ExternalMailAccount(dict):

    def __str__(self):
        return self['credentials']['username']


def _execfile(path):
    locals = {}
    with open(path, 'rb') as file:
        exec(compile(file.read(), path, 'exec'), globals(), locals)
    return locals


def load_config(directory):
    probes = []
    for path in glob.glob(os.path.join(directory, '*.py')):
        logging.info('loading configuration file %s', path)
        try:
            data = _execfile(path)
            probes.extend(data.get('PROBES', []))
        except Exception as e:
            logging.error('configuration error in %s: %s', path, e)
            continue
    logging.info('%d probes defined.', len(probes))
    return probes
