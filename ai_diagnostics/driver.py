import logging
import random
import threading
import time

log = logging.getLogger('driver')


class _ResultLog(object):
    """Keep around a limited number of probe results."""

    MAX_SIZE = 100

    def __init__(self):
        self.results = {}
        self.log = []

    def add(self, name, timestamp, result):
        key = (name, timestamp)
        self.results[key] = result
        self.log.append(key)
        while len(self.log) > self.MAX_SIZE:
            del self.results[self.log[0]]
            self.log = self.log[1:]

    def get(self, name, timestamp):
        return self.results[(name, timestamp)]

    def get_all(self):
        return [(x[0], x[1], self.results[x])
                for x in self.log[::-1]]


class Results(object):
    """Result cache that eventually forgets old results.
    
    The liveness threshold should be greater than the probing
    interval, or we're going to forget the most recent results.

    """

    def __init__(self, max_age=1800):
        self.max_age = max_age
        self.lock = threading.Lock()
        self.results = {}
        self.error_log = _ResultLog()

    def add(self, name, timestamp, result):
        with self.lock:
            if name in self.results:
                cur_ts = self.results[name][0]
                if timestamp < cur_ts:
                    return
            self.results[name] = (timestamp, result)
            if result.get('status') != 'ok':
                self.error_log.add(name, timestamp, result)

    def get_error(self, name, timestamp):
        with self.lock:
            return self.error_log.get(name, timestamp)

    def get_errors(self):
        with self.lock:
            return self.error_log.get_all()

    def get_results(self):
        cutoff = time.time() - self.max_age
        with self.lock:
            return dict((name, res[1]) for
                        name, res in self.results.items()
                        if res[0] > cutoff)

    def get_result(self, key):
        cutoff = time.time() - self.max_age
        timestamp, result = self.results[key]
        if timestamp < cutoff:
            return None
        return result


class _PeriodicWorker(threading.Thread):

    def __init__(self, fn, results, interval):
        super(_PeriodicWorker, self).__init__()
        self._fn = fn
        self._results = results
        self._interval = interval
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def _sleep(self, interval):
        log.info('%s: waiting for %g seconds', self._fn, interval)
        return self._stop_event.wait(timeout=interval)

    def _next_interval(self, ts):
        now = time.time()
        while ts < now:
            ts += self._interval
        return ts, ts - now

    def run(self):
        # Sleep a random amount of time before the first run to offset
        # all workers uniformly.
        if self._sleep(random.randint(0, self._interval)):
            log.info('%s: worker exiting', self._fn)
            return
        # Run periodically until stopped.
        ts = time.time()
        while True:
            log.info('%s: executing', self._fn)
            self._fn(self._results)

            ts, interval = self._next_interval(ts)
            if self._sleep(interval):
                log.info('%s: worker exiting', self._fn)
                return


class PeriodicExecutor(object):

    def __init__(self, results, probes, interval_secs=600):
        self.results = results
        self.threads = [
            _PeriodicWorker(probe, results, interval_secs)
            for probe in probes]
        for t in self.threads:
            t.start()

    def stop(self):
        for t in self.threads:
            t.stop()
        for t in self.threads:
            t.join()


class LinearExecutor(object):

    def __init__(self, results, probes):
        self.results = results
        self.probes = probes

    def run(self):
        for probe in self.probes:
            log.info('%s: executing', probe)
            probe(self.results)
