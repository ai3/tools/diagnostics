
from sleekxmpp import ClientXMPP


class SendMsgBot(ClientXMPP):
    """XMPP bot that gets online and optionally sends a message."""

    def __init__(self, jid, password, recipient=None, msg=None):
        super(SendMsgBot, self).__init__(jid, password)
        self.auto_reconnect = False
        self.reconnect_max_attempts = 0
        self.session_timeout = 5

        self.error = None
        self.recipient = recipient
        self.msg = msg

        self.add_event_handler('stream_error', self._error)
        self.add_event_handler('session_start', self._start)

    def _start(self, event):
        self.send_presence()
        self.get_roster()

        if self.recipient:
            self.send_message(mto=self.recipient, mbody=self.msg)

        self.disconnect()

    def _error(self, event):
        self.error = event
        self.disconnect()
