import binascii
import contextlib
import imaplib
import os
import smtplib
import ssl
import time
from email.utils import formatdate
from email.mime.text import MIMEText


IMAP_TIMEOUT = 30
SMTP_TIMEOUT = 30


class _SSLContext(ssl.SSLContext):
    """A SSLContext that forces a server_name for SNI."""

    def set_server_name(self, server_name):
        self._override_server_hostname = server_name

    def wrap_socket(self, *args, **kwargs):
        kwargs['server_hostname'] = self._override_server_hostname
        return super(_SSLContext, self).wrap_socket(*args, **kwargs)


@contextlib.contextmanager
def imap_conn(ctx, server_name, server_addr=None):
    if not server_addr:
        server_addr = server_name
    ssl_ctx = _SSLContext(ssl.PROTOCOL_TLSv1_2)
    ssl_ctx.load_default_certs()
    ssl_ctx.set_server_name(server_name)
    ctx.log('making IMAP connection to %s (%s)', server_name, server_addr)
    conn = imaplib.IMAP4_SSL(server_addr, ssl_context=ssl_ctx)
    conn.socket().settimeout(IMAP_TIMEOUT)
    try:
        yield conn
    finally:
        try:
            conn.close()
        except:
            pass


class Timeout(Exception):
    pass


def wait_for_unique_email(ctx, conn, message_id, timeout=300):
    def _check_for_msg():
        typ, data = conn.uid(
            'search', None, 'HEADER', 'Message-ID', message_id)
        if typ == 'OK' and data[0]:
            uid = data[0]
            typ, data = conn.uid('fetch', uid, '(RFC822)')
            if typ != 'OK':
                raise Exception('error fetching message: %s' % typ)
            msg = data[0][1]
            conn.uid('store', uid, '+FLAGS', r'(\Deleted)')
            conn.expunge()
            return msg
        return None

    ctx.log('waiting %d seconds for message %s to appear',
            timeout, message_id)
    deadline = time.time() + timeout
    while time.time() < deadline:
        msg = _check_for_msg()
        if msg:
            ctx.log('message %s found and removed successfully', message_id)
            return msg
        time.sleep(1)

    ctx.log('timeout waiting for message %s to appear', message_id)
    raise Timeout()


@contextlib.contextmanager
def smtp_conn(ctx, server_name, server_addr=None, port=587):
    if not server_addr:
        server_addr = server_name
    ssl_ctx = _SSLContext(ssl.PROTOCOL_TLSv1_2)
    ssl_ctx.load_default_certs()
    ssl_ctx.set_server_name(server_name)
    ctx.log('making SMTP connection to %s (%s)', server_name, server_addr)
    conn = smtplib.SMTP(server_addr, port, timeout=SMTP_TIMEOUT)
    conn.ehlo()
    conn.starttls(context=ssl_ctx)
    conn.ehlo()
    try:
        yield conn
    finally:
        conn.quit()


def _create_msg(sender, rcpt):
    unique_token = binascii.hexlify(os.urandom(8)).decode()
    message_id = '<test-%s@%s>' % (unique_token, sender.split('@')[1])
    msg = MIMEText('This is a test message, please ignore.')
    msg['Subject'] = '[diagnostics] Test message #%s' % unique_token
    msg['Message-ID'] = message_id
    msg['From'] = sender
    msg['To'] = rcpt
    msg['Date'] = formatdate()
    return msg, message_id


def send_test_email(ctx, conn, sender, rcpt):
    ctx.log('sending test email %s -> %s', sender, rcpt)
    msg, message_id = _create_msg(sender, rcpt)
    conn.sendmail(sender, [rcpt], msg.as_string())
    return message_id
