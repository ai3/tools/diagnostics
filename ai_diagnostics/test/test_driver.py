from ai_diagnostics.test import *


class DriverTest(unittest.TestCase):

    def setUp(self):
        self.results = Results()

    def tearDown(self):
        pass

    def test_parameterize(self):
        values = ['one', 'two', 'three']
        probes = parameterize(
            TEST_PROBES[0],
            'test_attr',
            values)
        for probe, value in zip(probes, values):
            self.assertEquals(probe.ctx['test_attr'], value)

    def test_run_probes(self):
        for p in TEST_PROBES:
            p(self.results)

        results = self.results.get_results()
        self.assertEquals(2, len(results))

        errs = self.results.get_errors()
        self.assertEquals(1, len(errs))
        err_name, err_ts, err_res = errs[0]
        self.assertEquals('err', err_name)
        self.assertEquals('error', err_res['status'])
