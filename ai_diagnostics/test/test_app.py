from ai_diagnostics.test import *
from ai_diagnostics.app import create_app


class AppTest(unittest.TestCase):

    def setUp(self):
        self.app, self.stop_fn = create_app({
            'DEBUG': True,
            'PROBES': TEST_PROBES,
        })
        self.c = self.app.test_client()

        for p in TEST_PROBES:
            p(self.app.results)

    def tearDown(self):
        self.stop_fn()

    def test_index(self):
        resp = self.c.get('/')
        self.assertEquals(200, resp.status_code)
        self.assertTrue(b'ok' in resp.data)
        self.assertTrue(b'err' in resp.data)

    def test_detail(self):
        resp = self.c.get('/detail/ok')
        self.assertEquals(200, resp.status_code)

    def test_errdetail(self):
        # Find the 'err' probe result timestamp to build the URL.
        err_ts = self.app.results.get_results()['err']['timestamp']
        resp = self.c.get('/errdetail/%d/err' % err_ts)
        self.assertEquals(200, resp.status_code)
