import unittest

from ai_diagnostics.driver import *
from ai_diagnostics.probe import *


def probe_ok(ctx):
    ctx.log('success')


def probe_err(ctx):
    raise Exception('failure')


TEST_PROBES = [
    Probe(probe_ok, 'ok', {}),
    Probe(probe_err, 'err', {}),
]
