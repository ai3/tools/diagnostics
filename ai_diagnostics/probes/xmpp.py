import threading
import time
from ai_diagnostics import xmpp


TIMEOUT = 30


def probe_xmpp(ctx):
    bot = xmpp.SendMsgBot(ctx['credentials']['username'],
                          ctx['credentials']['password'])
    bot.register_plugin('xep_0030')
    bot.register_plugin('xep_0199')

    # We can't reliably time out the XMPP probe on certain failures,
    # so we just shut down the client altogether after a timeout.
    done = threading.Event()

    def _timeout():
        time.sleep(TIMEOUT)
        if not done.isSet():
            ctx.log('timeout')
            bot.disconnect()
    threading.Thread(target=_timeout, daemon=True).start()

    try:
        ctx.log('connecting to %s:5222', ctx['server'])
        if bot.connect((ctx['server'], 5222), reattempt=False):
            ctx.log('connection successful')
            # should run the _start callback here.
            bot.process(block=True)
            ctx.log('disconnected successfully')
        else:
            ctx.log('unable to connect')
    finally:
        done.set()
        bot.disconnect()

    if bot.error:
        raise Exception('XMPP error: %s' % bot.error)
