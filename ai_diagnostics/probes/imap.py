from ai_diagnostics import mail


def probe_imap(ctx):
    with mail.imap_conn(ctx, ctx['server'], ctx.get('addr')) as conn:
        ctx.log('logging in as %s', ctx['credentials']['username'])
        conn.login(
            ctx['credentials']['username'],
            ctx['credentials']['password'])
        conn.select('INBOX')
        ctx.log('login successful, INBOX opened')
