from ai_diagnostics import browser


def probe_webmail(ctx):
    with browser.make_browser(ctx) as b:

        # Log in to 'accounts'.
        app_url = ctx['url']
        ctx.log('making initial request to %s', app_url)

        resp = browser.open_url(ctx, b, app_url)
        try:
            if resp.status_code != 200:
                raise Exception('Received HTTP status code %d, expected 200' % resp.status_code)
        finally:
            resp.close()

        # Go to the webmail page.
        ctx.log('switching over to the webmail')
        resp = b.follow_link('webmail.autistici.org')
        try:
            if resp.status_code != 200:
                raise Exception('Could not retrieve webmail page')
        finally:
            resp.close()

        # If it worked, then we can logout.
        ctx.log('validating successful rendering by looking for logout link')
        resp = b.follow_link('/?_task=logout')
        try:
            if resp.status_code != 200:
                raise Exception('Could not logout')
        finally:
            resp.close()
