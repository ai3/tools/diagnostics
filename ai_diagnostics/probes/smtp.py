from ai_diagnostics import mail


def _delivery(ctx, smtp_server, imap_server, smtp_username, smtp_password,
              imap_username, imap_password):
    """Sends an email from smtp_username to imap_username.

    Waits until the email is received by the target account.
    """
    ctx.log('sending mail from %s to %s', smtp_username, imap_username)
    with mail.smtp_conn(ctx, smtp_server) as smtp:
        smtp.login(smtp_username, smtp_password)
        msgid = mail.send_test_email(ctx, smtp, smtp_username, imap_username)

    ctx.log('waiting to receive email %s in %s/INBOX', msgid, imap_username)
    with mail.imap_conn(ctx, imap_server) as imap:
        imap.login(imap_username, imap_password)
        imap.select('INBOX')
        mail.wait_for_unique_email(ctx, imap, msgid)


def probe_mail_local_delivery(ctx):
    _delivery(
        ctx,
        ctx['smtp_server'],
        ctx['imap_server'],
        ctx['credentials']['username'],
        ctx['credentials']['password'],
        ctx['credentials']['username'],
        ctx['credentials']['password'],
    )


def probe_mail_incoming_delivery(ctx):
    _delivery(
        ctx,
        ctx['external_mail_account']['smtp_server'],
        ctx['imap_server'],
        ctx['external_mail_account']['credentials']['username'],
        ctx['external_mail_account']['credentials']['password'],
        ctx['credentials']['username'],
        ctx['credentials']['password'],
    )


def probe_mail_outbound_delivery(ctx):
    _delivery(
        ctx,
        ctx['smtp_server'],
        ctx['external_mail_account']['imap_server'],
        ctx['credentials']['username'],
        ctx['credentials']['password'],
        ctx['external_mail_account']['credentials']['username'],
        ctx['external_mail_account']['credentials']['password'],
    )
