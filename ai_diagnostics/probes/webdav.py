from ai_diagnostics import browser


def probe_webdav(ctx):
    with browser.make_session(ctx) as s:

        # Send an OPTIONS request to the DAV root URL.
        dav_url = ctx['url']
        ctx.log('making OPTIONS request to %s', dav_url)

        resp = s.options(dav_url,
                         auth=(
                             ctx['credentials']['username'],
                             ctx['credentials']['password'],
                         ))
        try:

            if resp.status_code != 200:
                raise Exception('Received HTTP status code %d, expected 200' % resp.status_code)
            ctx.log('request successful')

        finally:
            resp.close()
