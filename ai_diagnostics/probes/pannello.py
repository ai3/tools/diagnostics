from mechanicalsoup.utils import LinkNotFoundError
from ai_diagnostics import browser


def probe_pannello(ctx):
    with browser.make_browser(ctx) as b:

        app_url = ctx['url']
        ctx.log('making initial request to %s', app_url)

        resp = browser.open_url(ctx, b, app_url)
        try:
            if resp.status_code != 200:
                raise Exception('Received HTTP status code %d, expected 200' % resp.status_code)

            # See if we are successfully logged in by looking for the logout link.
            ctx.log('validating login by looking for logout link')
            try:
                b.get_current_page().find('a', title='Logout')
            except LinkNotFoundError:
                raise Exception('We did not successfully log in (unable to find logout link on the application page)')

        finally:
            resp.close()
