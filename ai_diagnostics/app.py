import logging
import signal
import time
from flask import Flask, render_template, abort
from cheroot.wsgi import Server, PathInfoDispatcher

try:
    from prometheus_client import make_wsgi_app
    has_prometheus = True
except ImportError:
    has_prometheus = False

from ai_diagnostics.driver import Results, PeriodicExecutor, LinearExecutor
from ai_diagnostics.config import load_config


app = Flask(__name__)


def _load_probes():
    if 'PROBES' in app.config:
        probes = app.config['PROBES']
    else:
        probes = load_config(app.config['PROBES_CONFIG_DIR'])
    return probes


def create_app(config={}):
    app.config.update(config)
    probes = _load_probes()
    probe_interval = app.config.get('PROBE_INTERVAL_SECS', 900)
    app.results = Results(
        max_age=app.config.get('RESULTS_MAX_AGE', probe_interval * 3))
    app.executor = PeriodicExecutor(
        app.results, probes, probe_interval)

    def _stop():
        app.executor.stop()

    return app, _stop


@app.route('/')
def home():
    return render_template(
        'index.html',
        errors=app.results.get_errors(),
        results=app.results.get_results(),
    )


@app.route('/detail/<path:probe_name>')
def probe_detail(probe_name):
    result = app.results.get_result(probe_name)
    if not result:
        abort(404)
    return render_template(
        'details.html',
        probe_name=probe_name,
        result=result,
    )


@app.route('/errdetail/<probe_ts>/<path:probe_name>')
def probe_error(probe_ts, probe_name):
    result = app.results.get_error(probe_name, int(probe_ts))
    if not result:
        abort(404)
    return render_template(
        'details.html',
        probe_name=probe_name,
        result=result,
    )


@app.template_filter('timefmt')
def timefmt(t):
    return time.strftime('%d/%m/%Y %H:%M', time.gmtime(t))


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('--config', metavar='PATH',
                        default='/etc/ai/diagnostics/',
                        help='directory with probe configurations (default %(default)s)')
    parser.add_argument('--addr', metavar='IP', default='127.0.0.1',
                        help='address to listen on (default %(default)s)')
    parser.add_argument('--port', metavar='N', type=int, default=8419,
                        help='TCP port to listen on (default %(default)s)')
    parser.add_argument('--interval', metavar='SECONDS', type=int, default=900,
                        help='interval between probe runs (default %(default)s secs)')
    parser.add_argument('--one-shot', action='store_true',
                        help='run all probes just once and exit')
    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('urllib3.connectionpool').setLevel(logging.ERROR)

    app.config.update({
        'PROBES_CONFIG_DIR': args.config,
        'PROBE_INTERVAL_SECS': args.interval,
    })

    if args.one_shot:
        probes = _load_probes()
        results = Results()
        LinearExecutor(results, probes).run()
        for k, v in results.get_results().items():
            print(f'{k} {v}')
        return

    # Create the application and start the CherryPY WSGI server.
    flask_app, cleanup = create_app()
    if has_prometheus:
        flask_app = PathInfoDispatcher({
            '/metrics': make_wsgi_app(),
            '/': flask_app,
        })

    server = Server(
        (args.addr, args.port),
        flask_app,
        numthreads=8,
    )

    # Nicely shut down on SIGTERM.
    def _sig_shutdown(signo, frame):
        server.stop()
    signal.signal(signal.SIGTERM, _sig_shutdown)

    try:
        logging.info('server starting on http://%s:%d/', args.addr, args.port)
        server.start()
    except KeyboardInterrupt:
        server.stop()

    logging.info('server shutdown')
    cleanup()


if __name__ == '__main__':
    main()
