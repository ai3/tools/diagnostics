FROM debian:buster

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        python3 python3-pip python3-setuptools python3-wheel

ADD . /tmp/src
WORKDIR /tmp/src
RUN python3 setup.py install

ENTRYPOINT ["/usr/local/bin/ai-diagnostics-server"]
