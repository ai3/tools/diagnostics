#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="ai_diagnostics",
    version="3.0",
    description="A/I Account Prober",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/diagnostics",
    install_requires=["cheroot", "Flask", "Mechanicalsoup", "sleekxmpp",
                      "prometheus_client"],
    packages=find_packages(),
    package_data={
        "ai_diagnostics": [
            "templates/*.html",
        ]},
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "ai-diagnostics-server = ai_diagnostics.app:main",
        ],
    },

)

