Application-level probes
===

**OBSOLETE**, now replaced by [/ai3/tools/service-prober](https://git.autistici.org/ai3/tools/service-prober)

This package contains a series of probes for application-level
functionality of the A/I services (email, web, etc), and a driver to
run them periodically and export their results to our monitoring
system.

The configuration for the probes takes the form of Python code, stored
in files in the *--config* directory (/etc/ai/diagnostics by default),
with a `.py` extension. These Python modules should define a top-level
`PROBES` variable containing the desired probes.

Probes should be instances of Probe objects, that compose the
individual probe functions defined in the [probes/](./probes/)
directory with different sets of parameters, stored in the *probe
context*. A simple configuration with a single probe could be:

```python
creds = Credentials(username='foo', password='bar')
PROBES = [
    Probe(probe_xmpp, 'jabber', {
        'server': 'jabber.autistici.org',
        'credentials': creds,
    })]
```

Running the same probe over a set of similar parameters can be
facilitated by the *parameterize* function, that creates list of
contexts by parameterizing a single attribute, for example:

```python
creds = [
    Credentials(username='user1', password='pass1'),
    Credentials(username='user2', password='pass2')]
PROBES = parameterize(
    Probe(
        probe_webmail,
        'webmail',
        {
            'url': 'https://webmail.example.com'
        }),
    'credentials',
    creds)
```

The above snippet will create two instances of the *probe_webmail*
probe, each pointing at the same URL but performing the login with
different test users.

Probes are named by concatenating the base name (here "webmail") with
the values passed to parameterize(), so in this case we would have

    webmail/credentials=user1
    webmail/credentials=user2

The Credentials object is just a subclass of *dict* that, when
stringified, only returns the username, so we can avoid having the
password in the probe names above.

The application runs a simple HTTP server with a */metrics* endpoint
for Prometheus scraping.

## Running probes locally for testing purposes

Start by creating a Python virtualenv to install the dependencies of
this repository, and creating a directory for your configuration:

```shell
$ virtualenv --python=python3 venv
$ ./venv/bin/python3 setup.py develop
$ mkdir config
```

Put whatever configuration you want to test in a `.py` file in the
*config* directory (look at the examples above for references), then
run all the probes right away without starting a web server:

```shell
$ ./venv/bin/ai-diagnostics-server --config=config --one-shot
```
